

Software Product: �**ReflFS**�
==========================

Specification of software requirements
--------------------------------------


Version 1
------------------------------------------------------------------------


Specification of software requirements
--------------------------------------


**1. Introduction**<br/>
This document is designed group 1 Team developers to describe software �ReflFS�. As well as system, functional requirements for the product.

**2. General description**<br/>
Purpose of the product - ReflFS.
ReflFS - is the plugin for Chrome OS, which allows to display the file system. The plugin allows you to delete, rename, copy, paste files also have the ability to create folders.

*Perspectives:* The product will be useful for ChromeOS users to work with the file system.

*Requirements:* Plugin is an application written in C ++ using the Native Client SDK. Requires installed ChromeOS.

**3. Specification requirements**<br/>
*Requirements for the development of this product:*<br/>
Development environment: CodeLite.<br/>
*Requires installed:* <br/>
Native Client SDK, VM VirtualBox with ChromeOS;

**3.1 Functional requirements**            
The software must provide ChromeOS users to work with the file system.

>  - Displays the entire file system.

 
The user should be able to:

>  - remove files/folders,
>  - copy, paste files/folders,
>  - sort files by date, name, type, size,
>  - create / delete folders,
>  - change the type of display files (icons, list)
>  - search for the file/folder on the file system,
>  - change the file/folder name,
>  - drag the files to a folder,
>  - cut files/folders,
>  - open files.

**4. Plugin interface**
1. Displaying by icons. 2. Displaying by list.
<p align="center"> <img src="/uploads/6f661eb42165851c2df3a1947c8609d6/prew.png" </p>

**5. User documentation requirements**<br/>
Requirements at this stage is absent.
 
**6. Licensing requirements**<br/>
Free software.             
 

Resources
---------
1. Issue tracker: https://gitlab.com/DeusD/ReflFS/issues
2. Sources: https://gitlab.com/DeusD/ReflFS
3. Wiki: https://gitlab.com/DeusD/ReflFS/wikis/home

Other info
---------
1. Icons - Designed by Freepik and distributed by Flaticon.
