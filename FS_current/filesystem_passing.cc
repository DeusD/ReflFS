
// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <string>
#include <vector>
#include <memory>
#include <strstream>
#include <time.h>

#include "json/json.h"
#include "IEvent.h"
#include "ppapi/c/pp_errors.h"
#include "ppapi/c/ppb_file_io.h"
#include "ppapi/cpp/file_io.h"
#include "ppapi/cpp/file_ref.h"
#include "ppapi/cpp/file_system.h"
#include "ppapi/cpp/instance.h"
#include "ppapi/cpp/module.h"
#include "ppapi/cpp/var.h"
#include "ppapi/cpp/var_dictionary.h"
#include "ppapi/utility/completion_callback_factory.h"
#include "ppapi/utility/threading/simple_thread.h"

#ifdef WIN32
#undef PostMessage
// Allow 'this' in initializer list
#pragma warning(disable : 4355)
#endif

namespace {
	const std::string readFile = "/Downloads/ls.txt";
};
class Instance : public pp::Instance {
 public:
  explicit Instance(PP_Instance instance)
      : pp::Instance(instance),
        callback_factory_(this),
        thread_(this) {}

 private:
  virtual bool Init(uint32_t /*argc*/,
                    const char* /*argn*/ [],
                    const char* /*argv*/ []) {
    thread_.Start();
    return true;
  }

  virtual void HandleMessage(const pp::Var& var_message) {
    if(var_message.is_string()){
           std::string contents = var_message.AsString();
		   std::string save_path = full_path + "/commandFromNaCl.json";

		    thread_.message_loop().PostWork(callback_factory_.NewCallback(
                &Instance::DoWork, filesystem, save_path, contents));
  	}

	else{
    	pp::VarDictionary var_dict(var_message);
    	pp::Resource filesystem_resource = var_dict.Get("filesystem").AsResource();
		filesystem = static_cast< std::shared_ptr<pp::FileSystem>
																  >(new pp::FileSystem(filesystem_resource));
    	full_path = var_dict.Get("fullPath").AsString();
        std::string contents = var_message.AsString();
    	std::string save_path = full_path + "/commandFromNaCl.txt";

    	thread_.message_loop().PostWork(callback_factory_.NewCallback(
        	&Instance::DoWork, filesystem, save_path, contents));
	}
  }

  void PostMessage(const char* str){
      pp::Instance::PostMessage(str);
  }

  void DoWork(int32_t /* result */,
                 std::shared_ptr<pp::FileSystem>& filesystem,
                 const std::string& path,
                 const std::string& contents) {
      int32_t result = 1;
      this->WriteFile(result, filesystem, path, contents);
      if(result == 0)
          return ;
      //place to write function
      this->ReadFile(result, filesystem);
      if(result == 0)
          return ;
  }


void WriteFile(int32_t  result,
                 std::shared_ptr<pp::FileSystem>& filesystem,
                 const std::string& path,
                 const std::string& contents) { // TODO(DeusD): add result
    int64_t offset = 0;
    int32_t bytes_written = 0;

    Delegate* delegate = new Delegate;
    delegate->Connect(this, &Instance::PostMessage);

    pp::FileRef ref(*filesystem, path.c_str());
    pp::FileIO file(this);

	int32_t open_result =
        file.Open(ref, PP_FILEOPENFLAG_WRITE | PP_FILEOPENFLAG_CREATE |
                           PP_FILEOPENFLAG_TRUNCATE,
                  pp::BlockUntilComplete());
    if (open_result != PP_OK) {
        result = 0;
        IEvent* error_event = new JsonEvent;
        error_event->send_error(delegate, "FATAL ERROR: Cannot open file");
        delete error_event;
        return ; 
    }

	bytes_written = file.Write(offset, contents.c_str(),
                                 contents.length(), pp::BlockUntilComplete());
      if (bytes_written > 0) {
        offset += bytes_written;
      } 
      else {
        result = 0;
        IEvent* error_event = new JsonEvent;
        error_event->send_error(delegate, "FATAL ERROR: Zero bytes written");
        delete error_event;
        return ; 
      }
    int32_t flush_result = file.Flush(pp::BlockUntilComplete());
    if (flush_result != PP_OK) {
        result = 0;
        IEvent* error_event = new JsonEvent;
        error_event->send_error(delegate, "FATAL ERROR: Cannot flush file");
        delete error_event;
        return ;
    }
    result = 1;
}

void ReadFile(int32_t status,
              std::shared_ptr<pp::FileSystem>& filesystem){

     Delegate* delegate = new Delegate;
     delegate->Connect(this, &Instance::PostMessage);

     pp::FileRef refread(*filesystem, readFile.c_str());
     pp::FileIO fileRead(this);

     clock_t try_open_start = clock();
     clock_t try_open_end = clock();
	 int32_t open_result = 0;
     do{
        open_result = fileRead.Open(refread, PP_FILEOPENFLAG_READ, pp::BlockUntilComplete());
     }while(open_result != PP_OK || try_open_end - try_open_start < 4);

     if (open_result != PP_OK) {
        status = 0;
        IEvent* error_event = new JsonEvent;
        error_event->send_error(delegate, "Sorry, timeout");
        delete error_event;
        return ;
     }
        int32_t bytes_read = 0;
        std::vector<char> result (4096);
        bytes_read = fileRead.Read(0, &result.at(0), result.size(), pp::BlockUntilComplete());
        if (bytes_read < 0){
            status = 0;
            IEvent* error_event = new JsonEvent;
            error_event->send_error(delegate, "Sorry, timeout");
            delete error_event;
            return;
        }
        result.resize(bytes_read);

        std::string resultString (result.begin(), result.begin() + bytes_read);
        PostMessage(resultString.c_str());
        status = 1;
}

 private:
  pp::CompletionCallbackFactory<Instance> callback_factory_;
  pp::SimpleThread thread_;
  std::shared_ptr<pp::FileSystem> filesystem;
  std::string full_path;
};

class Module : public pp::Module {
 public:
  Module() : pp::Module() {}
  virtual ~Module() {}

  virtual pp::Instance* CreateInstance(PP_Instance instance) {
    return new Instance(instance);
  }
};

namespace pp {
Module* CreateModule() { return new ::Module(); }
}  // namespace pp
