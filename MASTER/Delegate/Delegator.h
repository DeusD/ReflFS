#include <iostream>
#include <string>
#include <assert.h>

class IArguments{
    public:
        virtual ~IArguments(){}
};

template<class T> class Arguments : public IArguments{
public:
    T arg;
    Arguments(T i_arg): arg(i_arg) {}
};

//Container for templete params
class IContainer {
    public:
        virtual void call(IArguments*) = 0;
        virtual ~IContainer(){}
};

template<class T, class M> class Container: public IContainer {};

template<class T, class A> class Container<T, void (T::*)(A)>: public IContainer{
    using M =  void (T::*)(A);
    using A1 = Arguments<A>;
    T* m_class; //TODO(DeusD): go to the smart pointers everywhere
    M m_method;
public:
    Container(T* _class, M _method): m_class(_class), m_method(_method) {}
    void call(IArguments* i_arg) override {
        A1* args = reinterpret_cast<A1*>(i_arg);
        assert(args);
        if(args)
            (m_class->*m_method)(args->arg);
    }
};

//Delegate 
class Delegate{
private:
    IContainer* m_container;
    public:
    Delegate(): m_container(nullptr) {}
        template<class T, class M> void Connect(T* i_class, M i_method){
            if(m_container != nullptr)
                delete m_container;
            m_container = new Container<T, M>(i_class, i_method);
        }
        template<class T> void operator()(T arg){
            Arguments<T>* tmp = new Arguments<T>(arg);
            m_container->call(tmp);
            delete tmp;
        }
        ~Delegate(){
            if(m_container)
                delete m_container;
        }
};
