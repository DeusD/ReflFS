/* Copyright (c) 2012 The Chromium Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file. */

#include "queue.h"

#include <pthread.h>
#include <stdlib.h>

#include "ppapi/c/pp_var.h"

#define MAX_QUEUE_SIZE 16

//static pthread_mutex_t g_queue_mutex;

//static pthread_cond_t g_queue_not_empty_cond;

//static pp::Var g_queue[MAX_QUEUE_SIZE];

//static int g_queue_start = 0;

//static int g_queue_end = 0;

//static int g_queue_size = 0;



/** Initialize the message queue. */

queue::queue(): start(0), end(0), size(0){
}
void queue::InitializeMessageQueue() {
  pthread_mutex_init(&g_queue_mutex, NULL);
  pthread_cond_init(&g_queue_not_empty_cond, NULL);
}

int queue::IsQueueEmpty() { return size == 0; }

int queue::IsQueueFull() { return size == MAX_QUEUE_SIZE; }

int queue::EnqueueMessage(pp::Var message) {
  pthread_mutex_lock(&g_queue_mutex);

  if (IsQueueFull()) {
    pthread_mutex_unlock(&g_queue_mutex);
    return 0;
  }

  g_queue[end] = message;
  end = (end + 1) % MAX_QUEUE_SIZE;
  size++;

  pthread_cond_signal(&g_queue_not_empty_cond);

  pthread_mutex_unlock(&g_queue_mutex);

  return 1;
}

pp::Var queue::DequeueMessage() {
  pp::Var message;

  pthread_mutex_lock(&g_queue_mutex);

  while (IsQueueEmpty()) {
    pthread_cond_wait(&g_queue_not_empty_cond, &g_queue_mutex);
  }

  message = g_queue[start];
  start = (start + 1) % MAX_QUEUE_SIZE;
  size--;

  pthread_mutex_unlock(&g_queue_mutex);

  return message;
}
