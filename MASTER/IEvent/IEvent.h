#include <iostream>
#include "json/json.h"
#include "../Delegate/Delegator.h"

class IEvent{
public:
    virtual void send(Delegate*) = 0; 
    virtual void send_error(Delegate*, const std::string&) = 0;
    virtual std::string get_field(const std::string&) = 0;
    virtual std::string ToString() = 0;
    virtual ~IEvent(){}
};

class JsonEvent: public IEvent{
    Json::Value root;
public:
    JsonEvent(){}
    JsonEvent(const std::string& s){
        Json::Reader reader;
        reader.parse(s.c_str(), root);
    }
    virtual void send(Delegate* delegate) override{
        Json::FastWriter writer;
        std::string str = writer.write(root);
        (*delegate)(str.c_str());
    }
    virtual void send_error(Delegate* delegate, const std::string& message) override{
        Json::Value error;
        error["status"] = "false";
        error["title"] = message;
        Json::FastWriter writer;
        std::string str = writer.write(error);
        (*delegate)(str.c_str());
    }
    virtual std::string get_field(const std::string& key) override{
        return root.get(key, "").asString();
    }
    virtual std::string ToString() override{
        Json::FastWriter writer;
        return writer.write(root);
    }
    virtual ~JsonEvent(){}
};

