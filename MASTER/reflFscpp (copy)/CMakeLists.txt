cmake_minimum_required(VERSION 2.8)
project(reflFscpp)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -pthread -Wl,--no-as-needed")

set(BOOST_ROOT "/home/denis/Загрузки/boost_1_61_0")

set(BOOST_INCLUDES "/usr/lib/x86_64-linux-gnu")
#set(BOOST_INCLUDES "/home/evgeny/Desktop/boost_1_61_0/stage/lib/")
#set(BOOST_INCLUDES "/home/evgeny/Desktop/boost_1_61_0/bin.v2/libs/system/build/gcc-4.8/release/link-static/threading-multi")
set (Boost_USE_STATIC_LIBS ON)

find_package(Boost REQUIRED system thread)

include_directories (/home/denis/project/websocketpp)

include_directories(${Boost_INCLUDE_DIRS})

set(SOURCE_FILES main.cpp IObservable.h IObserver.h IEvent.h JsonEvent.h WSReciever.cpp WSReciever.h WorkeInspector.cpp WorkeInspector.h IWorker.h Worker.h Worker.cpp WSSender.cpp WSSender.h NaClSender.h NaClReciever.h NaClReciever.cpp)
add_executable(reflFscpp ${SOURCE_FILES})
target_link_libraries(reflFscpp ${Boost_LIBRARIES} jsoncpp)


#[[find_path(Jsoncpp_INCLUDE_DIR
#NAMES json/features.h
#PATH_SUFFIXES jsoncpp
#PATHS ${Jsoncpp_PKGCONF_INCLUDE_DIRS}
#)]]
#include_directories(${Jsoncpp_INCLUDE_DIR})
#target_link_libraries(reflFscpp ${Jsoncpp_LIBRARY})
