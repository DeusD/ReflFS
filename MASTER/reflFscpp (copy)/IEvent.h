//
// Created by michael on 07.08.16.
//

#ifndef REFLFSCPP_IEVENT_H
#define REFLFSCPP_IEVENT_H

#include <websocketpp/config/asio_no_tls.hpp>
#include <string>


typedef websocketpp::connection_hdl connection_hdl;

class IEvent {
public:

	IEvent()
	{}

    explicit IEvent(connection_hdl h)
        :hdl(h)
    {}

    virtual void getData(const char* key, std::string& value) = 0;
	
	virtual void setData(const char* key, std::string value) = 0;

    connection_hdl getHdlCon() const {
        return hdl;
    }

    virtual std::string toString() = 0;
protected:
    connection_hdl hdl;
};
#endif //REFLFSCPP_IEVENT_H
