//
// Created by michael on 07.08.16.
//

#ifndef REFLFSCPP_IOBSERVABLE_H
#define REFLFSCPP_IOBSERVABLE_H

#include "IEvent.h"

class IObservable {
protected:
    virtual void notifyObserver(std::shared_ptr<IEvent> event) = 0;
public:
	virtual ~IObservable(){}
};

#endif //REFLFSCPP_IOBSERVABLE_H
