//
// Created by michael on 07.08.16.
//

#ifndef REFLFSCPP_IOBSERVER_H
#define REFLFSCPP_IOBSERVER_H

#include "IEvent.h"


class IObserver {
public:
    virtual void HandleEvent(std::shared_ptr<IEvent> event) = 0;
};
#endif //REFLFSCPP_IOBSERVER_H
