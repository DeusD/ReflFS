//
// Created by michael on 09.08.16.
//

#ifndef REFLFSCPP_IWORKER_H
#define REFLFSCPP_IWORKER_H

#include <memory>
#include "IObservable.h"
#include "IObserver.h"
#include "WSSender.h"

#include <boost/algorithm/string/replace.hpp>

class IWorker: public IObservable
{
public:
	IWorker()
	{}	

    IWorker(std::shared_ptr<IObserver> ob)
        :observer(ob)
    {}

    virtual void work(std::shared_ptr<IEvent> event) = 0;
	virtual ~IWorker(){}

private:
    std::shared_ptr<IObserver> observer;
	
protected:
	void shieldSpace(std::string& str) {
		boost::replace_all(str, " ", "\\ ");
	}

    void notifyObserver(std::shared_ptr<IEvent> event)
    {
        observer->HandleEvent(event);
    }
};
#endif //REFLFSCPP_IWORKER_H
