//
// Created by michael on 08.08.16.
//

#ifndef REFLFSCPP_JSONEVENT_H
#define REFLFSCPP_JSONEVENT_H

#include "IEvent.h"
#include "json/json.h"
#include "json/value.h"
#include "json/writer.h"
#include "json/reader.h"
//#include "jsoncpp.cpp"

class JsonEvent: public IEvent{
public:
	explicit JsonEvent(connection_hdl hdl)
        :IEvent(hdl)
    {}

    JsonEvent(const std::string& msg, connection_hdl hdl)
        :IEvent(hdl)
    {
        Json::Reader reader;
        reader.parse(msg.c_str(), jsonValue);
    }
    JsonEvent(Json::Value& root):jsonValue(root)
	{}
    JsonEvent(Json::Value& root, connection_hdl hdl)
            :IEvent(hdl), jsonValue(root)
    {}

    void getData(const char* key, std::string& value)
    {
        value = jsonValue.get(key,"").asString();
		std::cout << "key: " << key << " value: " << value << std::endl;
    }
	
	void setData(const char* key, std::string value)
	{
		jsonValue[key] = value;
	std::cout << "SetData - key: " << key << " value: " << value << std::endl;
	}	

    std::string toString()
    {
        Json::FastWriter fastWriter;
        return fastWriter.write(jsonValue);
    }

private:
    Json::Value jsonValue;
};

#endif //REFLFSCPP_JSONEVENT_H
