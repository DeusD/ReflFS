#include "NaClReciever.h"
#include "NaClSender.h"
#include "json/json.h"
#include "JsonEvent.h"
#include <memory>

void NaClReciever::read(std::string in)
{
	Json::Reader reader;
        Json::Value root;
        while(root.get("title","Error").asString() == "Error"){
            std::ifstream ins(in.c_str());
            reader.parse(ins, root);
            ins.close();
        }      
        std::fstream clear_file(in.c_str(), std::ios::out);
        clear_file.close();
     	notifyObserver(std::shared_ptr<IEvent>(new JsonEvent(root)));
}

void NaClReciever::notifyObserver(std::shared_ptr<IEvent> event)
{
    observer->HandleEvent(event);
}
