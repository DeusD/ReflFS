
#ifndef REFLFSCPP_NACLRECIEVER_H
#define REFLFSCPP_NACLRECIEVER_H
#include "IObservable.h"
#include "IObserver.h"
class NaClReciever: public IObservable
{
public:
    explicit NaClReciever(std::shared_ptr<IObserver> workI)
				:observer(workI)
			{}

    ~NaClReciever()
	{}

    void read(std::string in);
protected:
    void notifyObserver(std::shared_ptr<IEvent> event);
    std::shared_ptr<IObserver> observer;
};

#endif //REFLFSCPP_NACLRECIEVER_H
