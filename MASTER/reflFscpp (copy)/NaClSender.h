
#ifndef REFLFSCPP_NACLSENDR_H
#define REFLFSCPP_NACLSENDR_H

#include "IObserver.h"
#include <fstream>

#define pathToNaClFile "~/Downloads/ls.txt"
 
class NaClSender: public IObserver {
public:
    NaClSender()
	{}

    void HandleEvent(std::shared_ptr<IEvent> event)
    {
		std::ofstream out(pathToNaClFile);
		out << event->toString();
	}
};

#endif //REFLFSCPP_NACLSENDR_H
