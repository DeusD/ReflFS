//
// Created by michael on 09.08.16.
//

#include "WSReciever.h"

WSReciever::WSReciever()
    :observer(std::shared_ptr<IObserver>(new WorkeInspector(std::shared_ptr<IObserver>(new WSSender))))
{
    r_server.set_access_channels(websocketpp::log::alevel::all);
    r_server.clear_access_channels(websocketpp::log::alevel::frame_payload);

    r_server.init_asio();

    r_server.set_message_handler(bind(&WSReciever::on_message, this, &r_server,::_1,::_2));

    /*r_server.listen(9002);

    r_server.start_accept();

    r_thread = websocketpp::lib::make_shared<websocketpp::lib::thread>(&server::run, &r_server);*/
}


void WSReciever::run()
{
    r_server.listen(9002);

    r_server.start_accept();

    r_server.run();
    //r_thread = websocketpp::lib::make_shared<websocketpp::lib::thread>(&server::run, &r_server);
}

WSReciever::~WSReciever()
{
	r_server.stop_listening();
    //r_thread->join();
}

void WSReciever::on_message(server* s, websocketpp::connection_hdl hdl, message_ptr msg)
{
    std::cout  << "message: " << msg->get_payload() << std::endl;
    if (msg->get_payload() == "stop") {
        s->stop_listening();
        return;
    }
    notifyObserver(std::shared_ptr<IEvent>(new JsonEvent(msg->get_payload(), hdl)));
}

void WSReciever::notifyObserver(std::shared_ptr<IEvent> event)
{
    observer->HandleEvent(event);
}
