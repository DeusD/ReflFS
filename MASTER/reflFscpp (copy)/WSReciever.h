//
// Created by michael on 09.08.16.
//

#ifndef REFLFSCPP_WSRECIEVER_H
#define REFLFSCPP_WSRECIEVER_H

#include "websocketpp/server.hpp"
#include <websocketpp/config/asio_no_tls.hpp>

#include "IObservable.h"
#include "IObserver.h"
#include "JsonEvent.h"
#include "WorkeInspector.h"
#include <memory>

typedef websocketpp::server< websocketpp::config::asio> server;
typedef server::message_ptr message_ptr;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

class WSReciever: public IObservable {
public:
    explicit WSReciever();

    ~WSReciever();

    void run();

protected:
    void notifyObserver(std::shared_ptr<IEvent> event);

private:
    server r_server;
    //websocketpp::lib::shared_ptr<websocketpp::lib::thread> r_thread;
    std::shared_ptr<IObserver> observer;

    void on_message(server* s, websocketpp::connection_hdl hdl, message_ptr msg);
};

#endif //REFLFSCPP_WSRECIEVER_H
