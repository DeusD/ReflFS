//
// Created by michael on 09.08.16.
//

#include "WSSender.h"

WSSender::WSSender()
{
    s_client.clear_access_channels(websocketpp::log::alevel::all);
    s_client.clear_error_channels(websocketpp::log::elevel::all);

    s_client.init_asio();
    s_client.start_perpetual();

    s_thread = websocketpp::lib::make_shared<websocketpp::lib::thread>(&client::run, &s_client);
}