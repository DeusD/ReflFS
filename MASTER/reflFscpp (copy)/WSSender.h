//
// Created by michael on 09.08.16.
//

#ifndef REFLFSCPP_WSSENDER_H
#define REFLFSCPP_WSSENDER_H

#include "websocketpp/client.hpp"
#include <websocketpp/config/asio_no_tls_client.hpp>

#include "IObserver.h"

typedef websocketpp::client< websocketpp::config::asio> client;

class WSSender: public IObserver {
public:
    WSSender();

    void HandleEvent(std::shared_ptr<IEvent> event)
    {
        s_client.send(event->getHdlCon(), event->toString(), websocketpp::frame::opcode::text);
    }

    ~WSSender()
    {
		s_client.stop_perpetual();
        s_thread->join();
    }

private:
    client s_client;
    websocketpp::lib::shared_ptr<websocketpp::lib::thread> s_thread;
};


#endif //REFLFSCPP_WSSENDER_H
