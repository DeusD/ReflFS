//
// Created by michael on 09.08.16.
//

#include "WorkeInspector.h"
WorkeInspector::WorkeInspector(std::shared_ptr<IObserver> obForWorker)
    :badWork(std::unique_ptr<IWorker>(new BadWorker(obForWorker)))
{
    workers.insert(std::pair<std::string, std::unique_ptr<IWorker>>("SHOW", std::unique_ptr<IWorker>(new ShowWorker(obForWorker))));
    workers.insert(std::pair<std::string, std::unique_ptr<IWorker>>("COPY", std::unique_ptr<IWorker>(new CopyWorker(obForWorker))));
    workers.insert(std::pair<std::string, std::unique_ptr<IWorker>>("DELETE", std::unique_ptr<IWorker>(new DelWorker(obForWorker))));
    workers.insert(std::pair<std::string, std::unique_ptr<IWorker>>("MOVE", std::unique_ptr<IWorker>(new MoveWorker(obForWorker))));
    workers.insert(std::pair<std::string, std::unique_ptr<IWorker>>("MKDIR", std::unique_ptr<IWorker>(new MkdirWorker(obForWorker))));
    //workers.insert(std::pair<std::string, std::unique_ptr<IWorker>>("SEARCH", new SearchWorker));
    workers.insert(std::pair<std::string, std::unique_ptr<IWorker>>("MKFILE", std::unique_ptr<IWorker>(new MkfileWorker(obForWorker))));
}

void WorkeInspector::HandleEvent(std::shared_ptr<IEvent> event)
{
    std::string key;
    event->getData("title", key);
	workMap::iterator  it = workers.find(key);
    if( it != workers.end() )
		workers[key]->work(event);
	else{		
        badWork->work(event);
	}			
}
