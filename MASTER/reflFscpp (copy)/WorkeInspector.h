//
// Created by michael on 09.08.16.
//

#ifndef REFLFSCPP_WORKEINSPECTOR_H
#define REFLFSCPP_WORKEINSPECTOR_H

#include <memory>
#include <map>
#include "IObserver.h"
#include "IWorker.h"
#include "Worker.h"


class WorkeInspector: public IObserver {
public:
	typedef std::map<std::string, std::unique_ptr<IWorker>> workMap;

    WorkeInspector(std::shared_ptr<IObserver> obForWorker);

    void HandleEvent(std::shared_ptr<IEvent> event);

private:
    workMap workers;
	std::unique_ptr<IWorker> badWork;
};


#endif //REFLFSCPP_WORKEINSPECTOR_H
