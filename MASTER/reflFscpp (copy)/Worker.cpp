//
// Created by michael on 09.08.16.
//

#include "Worker.h"
#include "JsonEvent.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

std::string bool_to_string(bool a)
{
    if(a==true)
        return "true";
        else
            return "false";
}
void ShowWorker::work(std::shared_ptr<IEvent> event) {
    dirent *entry;
    DIR* dir;
    std::string path;
    event->getData("path", path);
	shieldSpace(path);
    dir = opendir(path.c_str());
    Json::Value root, item;
    root["title"] = "SHOW";
    for (int i = 0; (entry = readdir(dir)) != NULL; )
    {
        if(entry->d_name[0]!='.')//выводим папки//не выводим с точкой, ибо это скрытые файлы
        {
            item["type"] = (entry->d_type == 4)? std::string("DIR") : std::string("FILE");	 
            item["name"] = std::string(entry->d_name);
            item["size"] =  std::to_string(GetFileSize(path + std::string(entry->d_name)));
            item["modiftime"] = getFileCreationTime(path + std::string(entry->d_name));
            root["result"][i] = item;
            ++i;
        }
    }
	if(item.empty())
        root["result"] = "empty";
    notifyObserver(std::shared_ptr<IEvent> (new JsonEvent(root, event->getHdlCon())));
}

std::string ShowWorker::getFileCreationTime(std::string path) {
    struct stat attr;
    stat(path.c_str(), &attr);
    return std::string(ctime(&attr.st_mtime));
}

long ShowWorker::GetFileSize(std::string filename)
{
    struct stat stat_buf;
    int rc = stat(filename.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

bool CopyWorker::returnCopy(std::string& source, std::string& target){
    struct dirent *entry;
    DIR* dir;
    dir = opendir(source.c_str());
    if (!dir)//на случай отсутствия доступа к папке
    {
        return false;
    }
    while ( (entry = readdir(dir)) != NULL)
    {
        if(entry->d_type==4&&entry->d_name[0]!='.')//выводим папки//не выводим с точкой, ибо это скрытые файлы
        {
            std::string newpath;//новая папка для поиска
            newpath=source+"/"+entry->d_name;
            std::string newtocopy;
            newtocopy=target+"/"+entry->d_name;
            mkdir(newtocopy.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
            this->returnCopy(newpath,newtocopy);//рекурсия
        }
    }
    closedir(dir);
    dir = opendir(source.c_str());
    while ( (entry = readdir(dir)) != NULL)
    {
        if(entry->d_type!=4&&entry->d_name[0]!='.')//теперь файлы
        {
            std::string newpath;//новая папка для поиска
            newpath=source+"/"+entry->d_name;
            std::string newtocopy;
            newtocopy=target+"/"+entry->d_name;
            std::ifstream src(newpath.c_str(), std::ios::binary);//исходник
            std::ofstream dst(newtocopy.c_str(), std::ios::binary);//копия
            dst << src.rdbuf();
            src.close();
            dst.close();
        }
    }
    closedir(dir);
    return true;
}

void CopyWorker::work(std::shared_ptr<IEvent> event) {
    std::shared_ptr<IEvent> newEvent(new JsonEvent(event->getHdlCon()));
    DIR* dir;
    std::string source, target;
    event->getData("source", source);
    event->getData("target", target);
	shieldSpace(source);
	shieldSpace(target);
    dir = opendir(source.c_str());
    newEvent->setData("title", "COPY");
    if(!dir){
        struct stat sb;
        if( stat(source.c_str(), &sb)==-1)
        {
            newEvent->setData("complete", "false");
        }
        else
        {
            std::ifstream src(source.c_str(), std::ios::binary);//исходник
            std::ofstream dst(target.c_str(), std::ios::binary);//копия
			if(stat(target.c_str(), &sb) != -1)
		    {		        
				dst << src.rdbuf();
            	src.close();
            	dst.close();
                newEvent->setData("complete", "true");
			}
			else
                newEvent->setData("complete", "false");
        }
        notifyObserver(newEvent);
    }
    else{
        std::string tmp = source;
		mkdir(target.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        dir=opendir(target.c_str());
        if(!dir)
        {
            newEvent->setData("complete", "false");
            notifyObserver(newEvent);
            return;
        }

        newEvent->setData("complete", bool_to_string(returnCopy(source,target)));
        notifyObserver(newEvent);

    }

}
bool DelWorker::returnDel(std::string& path){
    struct dirent *entry;
    DIR* dir;
    dir = opendir(path.c_str());
    if (!dir){
        return false;
    }
    while ( (entry = readdir(dir)) != NULL)
    {
        if(entry->d_type==4&&entry->d_name[0]!='.')//выводим папки//не выводим с точкой, ибо это скрытые файлы
        {
            std::string newpath;
            newpath=path+"/"+entry->d_name;
            returnDel(newpath);
        }
    }
    closedir(dir);
    dir = opendir(path.c_str());
    while ( (entry = readdir(dir)) != NULL)
    {
        if(entry->d_type!=4)//теперь файлы
        {
            std::string newpath;//новая папка для поиска
            newpath=path+"/"+entry->d_name;
            remove(newpath.c_str());
        }
    }
    closedir(dir);
    rmdir(path.c_str());
    return true;
}

void DelWorker::work(std::shared_ptr<IEvent> event) {//del /home/anger/dest.txt
    std::shared_ptr<IEvent> newEvent(new JsonEvent(event->getHdlCon()));
    DIR* dir;
    std::string path;
    event->getData("path", path);
	shieldSpace(path);
    dir = opendir(path.c_str());
    newEvent->setData("title","DELETE");
    if(!dir){        
        struct stat sb;
        if( stat(path.c_str(), &sb)==-1)
        {
            newEvent->setData("complete","false");
        }
        else
        {
            newEvent->setData("complete","true");
            remove(path.c_str());
        }
        notifyObserver(newEvent);
    }
    else{
        newEvent->setData("complete",bool_to_string(returnDel(path)));
        notifyObserver(newEvent);
    }
}

bool MoveWorker::returnCut(std::string& source, std::string& target){
	std::unique_ptr<CopyWorker> command = std::unique_ptr<CopyWorker>(new CopyWorker);
	struct stat sb;
    if( stat(source.c_str(), &sb)==-1)
	{
	    return false;
	}
    mkdir(target.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    bool rez = command->returnCopy(source, target);
	std::unique_ptr<DelWorker> command2 = std::unique_ptr<DelWorker>(new DelWorker);
    return command2->returnDel(source) && rez;
}

void MoveWorker::work(std::shared_ptr<IEvent> event) {
    std::shared_ptr<IEvent> newEvent(new JsonEvent(event->getHdlCon()));
    std::string source, target;
    event->getData("source", source);
    event->getData("target", target);
	shieldSpace(source);
	shieldSpace(target);
    DIR* dir;
    dir = opendir(source.c_str());
	struct stat sb;
    newEvent->setData("title","MOVE");
    if(!dir)
    {              
        if( stat(source.c_str(), &sb)==-1)
        {
            newEvent->setData("complete","false");
        }
        else
        {
            newEvent->setData("complete","true");
            rename(source.c_str(),target.c_str());
        }
        notifyObserver(newEvent);
    }
    else{
        newEvent->setData("complete",bool_to_string(returnCut(source, target) && (stat(target.c_str(),&sb)==-1)));
        notifyObserver(newEvent);
    }
}

bool MkdirWorker::returnMkdir(std::string& path){
    mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    struct stat sb;
    if(stat(path.c_str(), &sb)==-1)
    {
        return false;
    }
    return true;
}

void MkdirWorker::work(std::shared_ptr<IEvent> event) {
    std::shared_ptr<IEvent> newEvent(new JsonEvent(event->getHdlCon()));
    std::string path;
    event->getData("path", path);
	shieldSpace(path);
    newEvent->setData("title","MKDIR");
    newEvent->setData("complete",bool_to_string(returnMkdir(path)));
    notifyObserver(newEvent);
}

/*-----------------------------------------------------------------------------------------------*/

    /*void SearchWorker::returnSearch(vector<string>& parsed_command, ){
        
        struct dirent *entry;
        DIR* dir;
        char* tmppath=new char[200];
        strcpy(tmppath,parsed_command[0].c_str());
        char* tmpwhat=new char[200];
        strcpy(tmpwhat,parsed_command[1].c_str());
        dir = opendir(parsed_command[0].c_str());
        if (!dir)//на случай отсутствия доступа к папке
        {
            obj.complete=false;
            return obj;
        }
        while ( (entry = readdir(dir)) != NULL)
        {
            if(entry->d_type==4&&entry->d_name[0]!='.')//выводим папки//не выводим с точкой, ибо это скрытые файлы
            {
                if(strstr(entry->d_name,parsed_command[1].c_str()))//выводим только те, которые удовлетворяют условию поиска
                {
                    string out;
               //     if(entry->d_type==4)
                        out="DIR ";
                   // else
                   //     out="FILE ";
                    out+=entry->d_name;
                    obj.data.push_back(out);
                }
               vector<string> newvector;
               string newpath;//новая папка для поиска
               newpath=parsed_command[0]+"/"+entry->d_name;
               newvector.push_back(newpath);//заменяем наш текущий путь
               newvector.push_back(parsed_command[1]);//но то, что ищем - старое
               ToJs temp;
               temp=returnSearch(newvector);//рекурсия
               for(unsigned int i=0;i<temp.data.size();i++)
                    obj.data.push_back(temp.data[i]);
            }
        }
        closedir(dir);
        dir = opendir(parsed_command[0].c_str());
        while ( (entry = readdir(dir)) != NULL)
        {
            if(entry->d_type!=4&&entry->d_name[0]!='.')//теперь файлы
            {
                if(strstr(entry->d_name,parsed_command[1].c_str()))//нужные файлы
                {
                    string out;
                    out="FILE ";
                    out+=entry->d_name;
                    obj.data.push_back(out);
                }
            }
        }
        closedir(dir);
        obj.complete=true;
        return obj;
    }
    void SearchWorker::work(std::shared_ptr<IEvent> event) {//search /home/anger what_we_search
		std::string source, target;
    	event->getData("source", source);
    	event->getData("target", target);
        vector<string> p;
        p.push_back(source);
        p.push_back(target);
        returnSearch(p);
    }*/


void MkfileWorker::work(std::shared_ptr<IEvent> event)
{   
    std::shared_ptr<IEvent> newEvent(new JsonEvent(event->getHdlCon()));
	std::string path;
	event->getData("path", path);
	shieldSpace(path);
	newEvent->setData("title", "MKFILE");
	std::ofstream of(path.c_str());
    of.close();
    struct stat sb;   
    newEvent->setData("complete", (stat(path.c_str(), &sb) == -1)? "false" : "true");
	notifyObserver(newEvent);
}

void BadWorker::work(std::shared_ptr<IEvent> event){
    std::shared_ptr<IEvent> newEvent(new JsonEvent(event->getHdlCon()));
    newEvent->setData("title","BAD");
	newEvent->setData("complete","false");
	std::string title;
    event->getData("title", title);
	newEvent->setData("body", "Error. There isn't '" + title + "' operation.");	
    notifyObserver(newEvent);
}

