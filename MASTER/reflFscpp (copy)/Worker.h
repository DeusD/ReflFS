//
// Created by michael on 09.08.16.
//

#ifndef REFLFSCPP_WORKER_H
#define REFLFSCPP_WORKER_H

#include "IWorker.h"

#include <string>
#include <vector>
#include <dirent.h>
#include <unistd.h>

class ShowWorker : public IWorker{
public:
	ShowWorker()
	{}

    explicit ShowWorker(std::shared_ptr<IObserver> ob)
		:IWorker(ob)
	{}

    void work(std::shared_ptr<IEvent> event);
    virtual ~ShowWorker()
	{}

private:
    std::string getFileCreationTime(std::string path);
    long GetFileSize(std::string filename);
};

class CopyWorker : public IWorker{
public:

	CopyWorker()
	{}	

    explicit CopyWorker(std::shared_ptr<IObserver> ob)
		:IWorker(ob)
	{}

    virtual ~CopyWorker()
    {}

    virtual void work(std::shared_ptr<IEvent> event);
    bool returnCopy(std::string& source, std::string& target);
};

class DelWorker : public IWorker{
public:
	
	DelWorker()
	{}

    explicit DelWorker(std::shared_ptr<IObserver> ob)
		:IWorker(ob)
	{}

    virtual ~DelWorker()
    {}
    virtual void work(std::shared_ptr<IEvent> event);
    bool returnDel(std::string& path);
};

class MoveWorker : public IWorker{
public:

	MoveWorker()
	{}
	
    explicit MoveWorker(std::shared_ptr<IObserver> ob)
		:IWorker(ob)
	{}

    virtual ~MoveWorker()
    {}

    virtual void work(std::shared_ptr<IEvent> event);
    bool returnCut(std::string& source, std::string& target);
};

class MkdirWorker : public IWorker{
public:

	MkdirWorker()
	{}

    explicit MkdirWorker(std::shared_ptr<IObserver> ob)
		:IWorker(ob)
	{}

    virtual ~MkdirWorker()
    {}

    virtual void work(std::shared_ptr<IEvent> event);

    bool returnMkdir(std::string& path);
};

/*class SearchWorker : public IWorker{
public:
    explicit SearchWorker(std::shared_ptr<IObserver> ob)
		:IWorker(ob)
	{}

    virtual ~SearchWorker()
	{}

    virtual void work(std::shared_ptr<IEvent> event);
};*/

class MkfileWorker : public IWorker{
public:

	MkfileWorker()
	{}	

    explicit MkfileWorker(std::shared_ptr<IObserver> ob)
		:IWorker(ob)
	{}

    virtual ~MkfileWorker()
	{}

    virtual void work(std::shared_ptr<IEvent> event);
};

class BadWorker : public IWorker{
public:

	BadWorker()
	{}

	explicit BadWorker(std::shared_ptr<IObserver> ob)
		:IWorker(ob)
	{}

    virtual ~BadWorker()
	{}

    virtual void work(std::shared_ptr<IEvent> event);
};
#endif //REFLFSCPP_WORKER_H
