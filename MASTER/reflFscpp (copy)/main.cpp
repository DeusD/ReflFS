#include <iostream>
#include "NaClReciever.h"
#include "WorkeInspector.h"
#include "IObserver.h"
#include "NaClSender.h"

int main() {
    //WSReciever ws;
   //ws.run();
	std::shared_ptr<IObserver> workI = std::shared_ptr<IObserver>(new WorkeInspector(std::shared_ptr<IObserver>(new NaClSender)));
    NaClReciever nr(workI);
    while(1){
    nr.read("~/commandFromNaCl.txt");
    }
    return 0;
}
