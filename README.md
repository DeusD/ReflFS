ReflFS : File manager for CromeOS 
=================================================
ReflFS - is a plugin for Chrome OS, which allows you to display the file system. The plugin allows to delete , rename , copy, paste files also have the option to create folders.
<p align="center">
<img src="/uploads/6f661eb42165851c2df3a1947c8609d6/prew.png"
</p>

Resources
---------
1. Issue tracker: https://gitlab.com/DeusD/ReflFS/issues
2. Sources: https://gitlab.com/DeusD/ReflFS
3. Wiki: https://gitlab.com/DeusD/ReflFS/wikis/home

Other info
---------
1. Icons - Designed by Freepik and distributed by Flaticon.
