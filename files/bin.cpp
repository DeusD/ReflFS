#include <iostream>
#include <vector>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>
#include <fstream>
#include <string>

enum DEM
{
    DEM_ERROR = 0,
    DEM_SHOW,
    DEM_COPY,
    DEM_DEL,
    DEM_CUT,
    DEM_RENAME,
    DEM_MKDIR,
    DEM_SEARCH
};

namespace ERROR
{
    class CANT_OPEN
    {

    };
}

class operation{
    std::vector<std::string> oper;
public:
    operation(): oper({"show", "copy","delete","cut","rename","mkdir","search"})
    {

    }
    static void strparse( std::string& str,std::string& str2,std::string& curoper)
    {
        const char* s1=new char[80];
        const char* s3=new char[80];
        curoper=str.substr(0,str.find_first_of('/',0));//ok
        s1=curoper.c_str();
        if(curoper=="copy"||curoper=="cut"||curoper=="rename")
        {
            str2=str.substr(str.find_first_of(' '),str.length());
            str2=str2.substr(str2.find_first_of(str2[1]),str2.length());
        }
        s3=str.c_str();
        str=str.substr(str.find_first_of('/',0),str.length());
        s3=str.c_str();
        str=str.substr(0,str.find_first_of(' ',0));
         s3=str.c_str();
    }
    int parse_commd(const std::string& str)
    {
        for(int i = 0; i < oper.size(); ++i)
        {
            if(oper[i] == str)
                return i+1;
        }
    return DEM_ERROR;
    }
};

class ofile_wrapper
{
    std::ofstream file;
public:
    ofile_wrapper(const std::string& _file_name)
    {
        file.open(_file_name);
        if(!file.is_open())
        {
            std::cerr << "ERROR::CANT::OPEN::out\n";
            throw ERROR::CANT_OPEN();
        }
}

    std::ofstream& operator<<(const std::string& rhs)
    {
        file << rhs;
        return file;
    }

    virtual ~ofile_wrapper()
    {
        file.close();
    }
};

class dir_wrapper
{
    DIR* file_dir;
public:
    dir_wrapper(const std::string& _dir_name)
    {
        file_dir = opendir(_dir_name.c_str());
        if (!file_dir)
        {
            perror("diropen");
            throw ERROR::CANT_OPEN();
        }
    }
    void close()
    {
        closedir(file_dir);
    }
    void reopen(const std::string& _dir_name)
    {
        file_dir = opendir(_dir_name.c_str());
        if (!file_dir){
                perror("diropen");
                throw ERROR::CANT_OPEN();
        }

    }
    dir_wrapper(): file_dir(nullptr)
    {

    }
    void set_dirName(const std::string& name)
    {
        file_dir = opendir(name.c_str());
        if (!file_dir)
        {
            perror("diropen");
            throw ERROR::CANT_OPEN();
        }
}
    DIR* get_dir()
    {
        return file_dir;
    }
    virtual ~dir_wrapper()
    {
        closedir(file_dir);
    }
};

int main()
{
    int par=0;
    operation test;
    struct dirent *entry;
   while(true)
   {
    std::string curoper;//oper-operation
    std::string str,str2;//input string and path second
    std::ifstream fin("/home/anger/Desktop/pipe");
   // std::cin.clear();
   // std::cin.ignore(std::cin.rdbuf()->in_avail());
    std::getline(fin, str);
    operation::strparse(str,str2,curoper);
    switch(test.parse_commd(curoper))
    {

    case DEM_ERROR:
    {

    }

    case DEM_SHOW:
    {
        ofile_wrapper fout("/home/anger/Desktop/ls.txt");
        dir_wrapper dir(str); // empty construct

        while ( (entry = readdir(dir.get_dir())) != NULL)
        {
            if(entry->d_type==4&&entry->d_name[0]!='.')
            {
                fout <<"DIR ";
                fout << entry->d_name<< "\n";
            }
        }
        dir.close();
        dir.reopen(str);
        while ( (entry = readdir(dir.get_dir())) != NULL)
        {
            if(entry->d_type!=4&&entry->d_name[0]!='.')
            {
                fout <<"FILE ";
                fout << entry->d_name<< "\n";
            }
        }
        fin.close();
        break;
    }

    case DEM_COPY:
    {
        std::ifstream src(str.c_str(), std::ios::binary);
        std::ofstream dst(str2.c_str(), std::ios::binary);
        dst << src.rdbuf();
        break;
    }

    case DEM_DEL:
    {
        if( remove(str.c_str()) != 0 )
            perror( "Error deleting file" );
        break;
    }

    case DEM_CUT:
    {
        rename(str.c_str(),str2.c_str());
        break;
    }

    case DEM_RENAME:
    {
        rename(str.c_str(),str2.c_str());
        break;
    }

    case DEM_MKDIR:
    {
        const int dir_err = mkdir(str.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        if (-1 == dir_err)
        {
            printf("Error creating directory!n");
            exit(1);
        }
        break;
    }

    case DEM_SEARCH:
    {
        break;
    }

    }
   }

}
