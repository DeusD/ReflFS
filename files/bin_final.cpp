#include <vector>//для разбития введённой команды на подстроки
#include <sys/stat.h>//для mkdir
#include <dirent.h>//opendir...
#include <fstream>//ifstream,ofstream
#include <cstring>//strstr...


using namespace std;

class IPipeFile{//интерфейс IPipeFile с чисто виртуальным read(ifstream& in)
public:
    virtual vector<string> read(ifstream& in) = 0;//получаем поток для чтения в качестве параметра
    virtual ~IPipeFile(){};
};

class ICommand{
public:
    virtual void execute(vector<string>& parsed_command, char* path) = 0;
    virtual ~ICommand(){};
};

class IOperation{
public:
    virtual void Do(vector<string> parsed_command) = 0;
    virtual ~IOperation(){};
};

class PipeFile : public IPipeFile{
private:
//PipeFile(const PipeFile&) = delete; //forbid copy construct
//void operation=(const PipeFile&) = delete; //forbid "=" operation
public:
    PipeFile(){

    }
    vector<string> read(ifstream& in){//читаем и парсим команду
        string command;//исходник
        vector<string> parsed_command;//результат
        getline(in,command);//чтение из потока команды
        parsed_command.push_back(command.substr(0,command.find_first_of(' ',0)));//записываем команду в нулевой элемент результирующего вектора
        while(command.find_first_of(' ')!=-1){//бесконечный цикл для разделения команд по пробелу
            command=command.substr(command.find_first_of(' ',0)+1,command.length());
            parsed_command.push_back(command.substr(0,command.find_first_of(' ')));
        }
        in.close();//закрываем поток команд до выполнения текущей команды
        return parsed_command;
    }
};

class ShowCommand : public ICommand{//отображение содержимого папки
private:
//ShowCommand(const ShowCommand&) = delete; //forbid copy construct
//void operation=(const ShowCommand&) = delete; //forbid "=" operation
public:
    ShowCommand(){

    }
    void execute(vector<string>& parsed_command, char* path){//show /home/user/Desktop
        ofstream out(path);//поток для вывода содержимого папки
        struct dirent *entry;
        DIR* dir;
        dir = opendir(parsed_command[0].c_str());
        while ( (entry = readdir(dir)) != NULL)
        {
            if(entry->d_type==4&&entry->d_name[0]!='.')//выводим папки//не выводим с точкой, ибо это скрытые файлы
            {
                out <<"DIR ";
                out << entry->d_name<< "\n";
            }
        }

        closedir(dir);
        dir = opendir(parsed_command[0].c_str());

        while ( (entry = readdir(dir)) != NULL)
        {
            if(entry->d_type!=4&&entry->d_name[0]!='.')//теперь файлы
            {
               // struct stat file_stat;
              //  stat(entry->d_name, &file_stat);
                out <<"FILE ";
                out << entry->d_name<< "\n";//<<" "<<file_stat.st_size
            }
        }
        out.close();//закрываем вывод для сохранения изменений
    }
};

class CopyCommand : public ICommand{
private:
//CopyCommand(const CopyCommand&) = delete; //forbid copy construct
//void operation=(const CopyCommand&) = delete; //forbid "=" operation
public:
    CopyCommand(){

    }
    void execute(vector<string>& parsed_command, char* path){//copy /home/anger/source.txt /home/dest.txt
        ifstream src(parsed_command[0].c_str(), ios::binary);//исходник
        ofstream dst(parsed_command[1].c_str(), ios::binary);//копия
        ofstream progress(path);//вывод прогресса
        dst << src.rdbuf();
        progress<<"Copy completed!";
        progress.close();
        src.close();
        dst.close();
    }
};

class DelCommand : public ICommand{
private:
//DelCommand(const DelCommand&) = delete; //forbid copy construct
//void operation=(const DelCommand&) = delete; //forbid "=" operation
public:
    DelCommand(){

    }
    void execute(vector<string>& parsed_command, char* path){//del /home/anger/dest.txt
        remove(parsed_command[0].c_str());
        ofstream out(path);
        out<<"Delete complete!";
        out.close();

    }
};

class CutCommand : public ICommand{
private:
//CutCommand(const CutCommand&) = delete; //forbid copy construct
//void operation=(const CutCommand&) = delete; //forbid "=" operation
public:
    CutCommand(){

    }
    void execute(vector<string>& parsed_command, char* path){//cut /home/anger/source.txt /home/dest.txt
         rename(parsed_command[0].c_str(),parsed_command[1].c_str());
         ofstream out(path);
         out<<"Cut complete!";
         out.close();
    }
};

class RenameCommand : public ICommand{
private:
//RenameCommand(const RenameCommand&) = delete; //forbid copy construct
//void operation=(const RenameCommand&) = delete; //forbid "=" operation
public:
    RenameCommand(){

    }
    void execute(vector<string>& parsed_command, char* path){//rename /home/anger/source.txt /home/dest.txt
        rename(parsed_command[0].c_str(),parsed_command[1].c_str());
        ofstream out(path);
        out<<"Rename complete!";
        out.close();
    }
};

class MkdirCommand : public ICommand{
private:
//MkdirCommand(const MkdirCommand&) = delete; //forbid copy construct
//void operation=(const MkdirCommand&) = delete; //forbid "=" operation
public:
    MkdirCommand(){

    }
    void execute(vector<string>& parsed_command, char* path){//mkdir /home/anger/dest
        mkdir(parsed_command[0].c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        ofstream out(path);
        out<<"Dir created!";
        out.close();
    }
};

class SearchCommand : public ICommand{
private:
//SearchCommand(const SearchCommand&) = delete; //forbid copy construct
//void operation=(const SearchCommand&) = delete; //forbid "=" operation
public:
    SearchCommand(){

    }
    void execute(vector<string>& parsed_command, char* path){//search /home/anger what_we_search
        ofstream out(path,ios_base::app);//поток для вывода содержимого папки
        struct dirent *entry;
        DIR* dir;
        dir = opendir(parsed_command[0].c_str());
        if (!dir)//на случай отсутствия доступа к папке
            return;
        while ( (entry = readdir(dir)) != NULL)
        {
            if(entry->d_type==4&&entry->d_name[0]!='.')//выводим папки//не выводим с точкой, ибо это скрытые файлы
            {
                if(strstr(entry->d_name,parsed_command[1].c_str()))//выводим только те, которые удовлетворяют условию поиска
                {
                    out <<"DIR ";
                    out << entry->d_name<<" "<<parsed_command[0].c_str()<< "\n";
                }
               vector<string> newvector;
               char* newpath=new char[500];//новая папка для поиска
               strcpy(newpath,parsed_command[0].c_str());
               strcat(newpath,"/");
               strcat(newpath,entry->d_name);
               newvector.push_back(newpath);//заменяем наш текущий путь
               newvector.push_back(parsed_command[1]);//но то, что ищем - старое
               execute(newvector,path);//рекурсия
               delete newpath;//отработали вложенную папку
            }
        }

        closedir(dir);
        dir = opendir(path);
        if (!dir)//на случай отсутствия доступа к папке
            return;
        while ( (entry = readdir(dir)) != NULL)
        {
            if(entry->d_type!=4&&entry->d_name[0]!='.')//теперь файлы
            {
                if(strstr(entry->d_name,parsed_command[1].c_str()))//нужные файлы
                {
                    out <<"FILE ";
                    out << entry->d_name<<" "<<parsed_command[0].c_str()<< "\n";
                }
            }
        }
        out.close();//закрываем вывод для сохранения изменений
    }
};

class Operation : public IOperation{
protected:
    ICommand* command;
    char* path;
public:
    Operation(){

    }
    void setpath(char* pth){//путь к файлу записи информации
        path=pth;
    }
    void Do(vector<string> parsed_command){//вызываем требуемый конструктор и функцию
        bool not_to_do=false;
        if(strstr(parsed_command[0].c_str(),"show"))
            command=new ShowCommand();
            else if(strstr(parsed_command[0].c_str(),"copy"))
                command=new CopyCommand();
                else if(strstr(parsed_command[0].c_str(),"delete"))
                    command=new DelCommand();
                    else if(strstr(parsed_command[0].c_str(),"cut"))
                        command=new CutCommand();
                        else if(strstr(parsed_command[0].c_str(),"rename"))
                            command=new RenameCommand();
                            else if(strstr(parsed_command[0].c_str(),"mkdir"))
                                command=new MkdirCommand();
                                else if(strstr(parsed_command[0].c_str(),"search"))
                                   {
                                       ofstream out(path,ios_base::trunc);//на случай того, что там уже что-то есть
                                       out.close();
                                       command=new SearchCommand();
                                   }
                                    else
                                        not_to_do=true;//неккоректный ввод команды

        if(!not_to_do)
        {
            parsed_command.erase(parsed_command.begin());
            command->execute(parsed_command,path);//вызываем команду
        }
    }
};

int main(){
    PipeFile* p;
    p=new PipeFile;
    while(1){
        Operation *oper;//внутри цикла, т.к. иначе нужно очищать поток ввода
        oper=new Operation;
        oper->setpath((char*)"/home/user/Desktop/ls.txt");//путь для записи вывода
        ifstream in("/home/user/Desktop/pipe");//путь для чтения
        oper->Do(p->read(in));
    }
}
