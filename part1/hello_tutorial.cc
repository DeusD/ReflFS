#include "json/json.h"
#include "Delegator.h"
#include "IEvent.h"
//#include <exception>
#include <sstream>
#include <fstream>
#include <iostream>
#include "ppapi/utility/completion_callback_factory.h"
#include "ppapi/utility/threading/simple_thread.h"
#include "ppapi/cpp/instance.h"
#include "ppapi/cpp/module.h"
#include "ppapi/cpp/var.h"

using namespace pp;

class fff{
	Delegate* delegate;
    public:
	fff(Delegate* _delegate): delegate(_delegate){}
    void ff(const char* str){
		(*delegate)(str);
    }
};

namespace {
const char* const kHelloString = "hello";
const char* const kReplyString = "hello from NaCl";
} // namespace

class HelloTutorialInstance : public pp::Instance {
 private:
     pp::CompletionCallbackFactory<HelloTutorialInstance> callback_factory;
     pp::SimpleThread thread;
     virtual bool Init(uint32_t , 
                        const char* [], 
                        const char* []){
         thread.Start();
         return true;
     }
 public:
  explicit HelloTutorialInstance(PP_Instance instance) : pp::Instance(instance), callback_factory(this),
    thread(this){}
  virtual ~HelloTutorialInstance() {}

  virtual void HandleMessage(const pp::Var& var_message) {
    // TODO(sdk_user): 1. Make this function handle the incoming message.
        PostMessage("Handle Message Started");
        IEvent* event = new JsonEvent(var_message.AsString());
        thread.message_loop().PostWork(callback_factory.NewCallback(&HelloTutorialInstance::print_json,
                    event));
        thread.Join();
        PostMessage("print_json ended");
        //thread.message_loop().PostWork(callback_factory.NewCallback(&HelloTutorialInstance::message));
  }

  void PostMessage(const char* var){
	  pp::Instance::PostMessage(var);
  }
  void print_json(int32_t result, IEvent* event){
	  Delegate* test_del = new Delegate;
	  test_del->Connect(this, &HelloTutorialInstance::PostMessage);
      event->send_error(test_del, "You are too fat to use this app");
	  PostMessage("content sended");
      int32_t rr = 0;
      this->message(rr);
  }
  void message(int32_t result){
      PostMessage("message");
  }
};
 
/// The Module class.  The browser calls the CreateInstance() method to create
/// an instance of your NaCl module on the web page.  The browser creates a new
/// instance for each <embed> tag with type="application/x-pnacl".
class HelloTutorialModule : public pp::Module {
 public:
  HelloTutorialModule() : pp::Module() {}
  virtual ~HelloTutorialModule() {}

  /// Create and return a HelloTutorialInstance object.
  /// @param[in] instance The browser-side instance.
  /// @return the plugin-side instance.
  virtual pp::Instance* CreateInstance(PP_Instance instance) {
    return new HelloTutorialInstance(instance);
  }
};

namespace pp {
/// Factory function called by the browser when the module is first loaded.
/// The browser keeps a singleton of this module.  It calls the
/// CreateInstance() method on the object you return to make instances.  There
/// is one instance per <embed> tag on the page.  This is the main binding
/// point for your NaCl module with the browser.
Module* CreateModule() {
  return new HelloTutorialModule();
}
}  // namespace pp
