# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/denis/project/ReflFS/sample_part1/Utests/hello_tutorial.cc" "/home/denis/project/ReflFS/sample_part1/Utests/build/CMakeFiles/runTests.dir/hello_tutorial.cc.o"
  "/home/denis/project/ReflFS/sample_part1/Utests/main_test.cpp" "/home/denis/project/ReflFS/sample_part1/Utests/build/CMakeFiles/runTests.dir/main_test.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "Clang")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
