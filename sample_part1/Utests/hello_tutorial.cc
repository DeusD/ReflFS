// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/// @file hello_tutorial.cc
/// This example demonstrates loading, running and scripting a very simple NaCl
/// module.  To load the NaCl module, the browser first looks for the
/// CreateModule() factory method (at the end of this file).  It calls
/// CreateModule() once to load the module code.  After the code is loaded,
/// CreateModule() is not called again.
///
/// Once the code is loaded, the browser calls the CreateInstance()
/// method on the object returned by CreateModule().  It calls CreateInstance()
/// each time it encounters an <embed> tag that references your NaCl module.
///
/// The browser can talk to your NaCl module via the postMessage() Javascript
/// function.  When you call postMessage() on your NaCl module from the browser,
/// this becomes a call to the HandleMessage() method of your pp::Instance
/// subclass.  You can send messages back to the browser by calling the
/// PostMessage() method on your pp::Instance.  Note that these two methods
/// (postMessage() in Javascript and PostMessage() in C++) are asynchronous.
/// This means they return immediately - there is no waiting for the message
/// to be handled.  This has implications in your program design, particularly
/// when mutating property values that are exposed to both the browser and the
/// NaCl module.

#include <string>
#include <gtest/gtest.h>
#include "stubInstance.h"
#include "stubVar.h"

namespace {
const char* const kHelloString = "hello";
const char* const kReplyString = "hello";
}

class HelloTutorialInstance: public Instance{
 public:
	 HelloTutorialInstance(){;}
  explicit HelloTutorialInstance(PP_Instance instance) : Instance(instance)
  {}
  virtual std::string HandleMessage(const Var& var_message) {
    if (!var_message.is_string())
    	return "";
    std::string message = var_message.AsString();
    Var var_reply;
    if (message == kHelloString) {
       var_reply = Var(kReplyString);
	   return var_reply.AsString();
    }
	return "";
  }
  virtual ~HelloTutorialInstance() {}
};

/*class HelloTutorialModule : public pp::Module {
 public:
  HelloTutorialModule() : pp::Module() {}
  virtual ~HelloTutorialModule() {}

  virtual pp::Instance* CreateInstance(PP_Instance instance) {
    return new HelloTutorialInstance(instance);
  }
};

namespace pp {
Module* CreateModule() {
  return new HelloTutorialModule();
}
}
*/
