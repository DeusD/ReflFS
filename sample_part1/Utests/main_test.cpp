#include <gtest/gtest.h>
#include "stubVar.h"
#include "hello_tutorial.cc"

TEST(HandleMessage, IsHelloReturned){
	Var var;
	HelloTutorialInstance hel;
	EXPECT_EQ("hello", hel.HandleMessage(var));
}

GTEST_API_ int main(int argc, char **argv){
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
	}
