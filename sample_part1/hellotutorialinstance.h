#ifndef HELLOTUTORIALINSTANCE_H
#define HELLOTUTORIALINSTANCE_H


#include "ppapi/cpp/instance.h"
#include "ppapi/cpp/module.h"
#include "ppapi/cpp/var.h"

class IPostMessage
{
public:
    virtual ~IPostMessage(){}
    virtual void PostMessage(const pp::Var& message) = 0;
};

class InstanceInterface : public pp::Instance {
    public:

    explicit InstanceInterface(PP_Instance inst) :
        pp::Instance(inst),
        stub(nullptr)
    {
        printf("Instance constructor \n");
    }

    explicit InstanceInterface(PP_Instance inst, std::auto_ptr<IPostMessage> postMessageStub) :
        pp::Instance(inst),
        stub(postMessageStub)
    {
        printf("Instance constructor with stub\n");
    }

    virtual void PostMessage(const pp::Var& message)
    {
        printf("Post Message : \n");
        if (stub.get() != nullptr)
        {
            stub->PostMessage(message);
        }
        else
        {
            pp::Instance::PostMessage(message);
        }
    }

private:
    std::auto_ptr<IPostMessage> stub;
};

/// The Instance class.  One of these exists for each instance of your NaCl
/// module on the web page.  The browser will ask the Module object to create
/// a new Instance for each occurrence of the <embed> tag that has these
/// attributes:
///     src="hello_tutorial.nmf"
///     type="application/x-pnacl"
/// To communicate with the browser, you must override HandleMessage() to
/// receive messages from the browser, and use PostMessage() to send messages
/// back to the browser.  Note that this interface is asynchronous.
class HelloTutorialInstance : public InstanceInterface {
 public:
  /// The constructor creates the plugin-side instance.
  /// @param[in] instance the handle to the browser-side plugin instance.
  explicit HelloTutorialInstance(PP_Instance instance);
  explicit HelloTutorialInstance(PP_Instance instance, std::auto_ptr<IPostMessage> postMessageStub);
  virtual ~HelloTutorialInstance();

  /// Handler for messages coming in from the browser via postMessage().  The
  /// @a var_message can contain be any pp:Var type; for example int, string
  /// Array or Dictinary. Please see the pp:Var documentation for more details.
  /// @param[in] var_message The message posted by the browser.
  virtual void HandleMessage(const pp::Var& var_message);

};

#endif // HELLOTUTORIALINSTANCE_H